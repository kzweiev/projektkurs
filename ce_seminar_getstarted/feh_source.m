function Qv=feh_source(prb,qv)

% function Qv=FEH_SOURCE(prb,qv)
% computes the coupling blocks for stranded conductors
%
% input parameters
%    prb        : FEMM problem data structure
%    qv         : [W/m^3]  : volumetric heat generation
%
% output parameters
%    Qv        : [W]       : heat generation matrix
%
% see also DIVGRAD, CURLCURL, FEM_SOURCE

numelem  = size(prb.elem,1);
numnode  = size(prb.node,1);

[a,b,c,area,r,z]=linear_shape_functions(prb);
if strcmp(prb.ProblemType,'axisymmetric')
  depth=2*pi*mean(r,2);
else
  depth=prb.Depth*ones(numelem,1);
end
if size(qv,1)~=numelem
  elemregi = prb.blocklabels(prb.elem(:,4),3);
  qv = qv(elemregi);
end

Qv = zeros(length(prb.node),1);
for k=1:numelem
  bl  = prb.elem(k,4);
  idx = prb.elem(k,1:3);
  %------------------------------------------------------------------------
  % IMPLEMENTATION POINT - Done!
  % compute the elementary heat excitation vector
  %------------------------------------------------------------------------
  Qv(idx)=Qv(idx)+(1/3)*qv(k)*area(k)*ones(3,1)*depth(k);
end
