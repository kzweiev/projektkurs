% Projektkurs CE 2013
% Institut Theorie Elektromagnetischer Felder, Prof. Dr. Sebastian Schöps
% Martin Lilienthal, M.Sc. – Dipl.-Phys. Fritz Kretzschmar – Klaus Klopfer, M.Sc. – Dipl.-Ing. Uwe Niedermayer
%
% basierend auf der Vorlesungsreihe "Electromagnetic Field Simulation for Electrical Energy Applications"
% von Prof. De Gersem, Aachen


%Input Paramters: 
%% metallKoerper int:  0 no compound, 1 ferrite ring, 2 brass ring
%% choose Abstand between 19 and 30
%% size: length in radial direction, min=70

%Return Values:
%% Struct prb2 containing the fields:
%% max_temp_regions:  Maximum temperature for each region
%% T: Temperature on each node
%% elem: nodes and blocklabel for each elem
%% nodes: coordinates and magnetic vector potential
%% E:  electric field for each node
%% A: magnetic vector potential for each node
%% B: magnetic flux for each elem
%% regions: names of the regions
%% error: error in the MQS Simulation compared to femm (norm of difference vector of a_phasors)
%% Q: the HeatFlux for each elem
%% J: current density for each elem


function prb2=runmeEval(metallKoerper, Abstand , Size )

% build automatically femm-model with input parameters
if nargin==0
build;
end
if nargin==1
build(metallKoerper);
end
if nargin==2
build(metallKoerper, Abstand);
end
if nargin==3
build(metallKoerper, Abstand , Size );
end

% get data from FEMM
path='models\femm_modell2.ans';
prb      = read_femm(path,0);                                  % []     :  read problem ".ans"-file
prb = regiq(prb);

% parameters
freq     = sscanf(prb.Frequency,'%f');                                     % [Hz]   :  frequency from FEMM
omega    = 2*pi*freq;                                                      % [Hz]   :  angular frequency

% construct matrices of MQS
Kmag     = fem_curlcurl(prb,prb.regirelu);                                 % [1/H]  :  curl-curl matrix
Mmag     = fem_edgemass(prb,prb.regicond);                                 % [S]    :  matrix of conductivity
[Pmag, J_elem]     = fem_source(prb);                                      % [ ]    :  distribution matrix (for currents)

% set boundary conditions (each side)
idxbdry  = bdrycond_getidx(prb,0,1);                                         %[#]    :  get boundary indices
%------------------------------------------------------------------------
% IMPLEMENTATION POINT - DONE!
% compute the indices of degrees of freedom
%------------------------------------------------------------------------
% [#]    :  compute indices of degrees of freedom 
% idxdof are all nodes except for idxbdry
idxdof=[1:prb.numnode];
for boundarynode = idxbdry'
	idxdof=idxdof(idxdof~=boundarynode);
end

%reduce the system to the degrees of freedom
Kmagsh   = Kmag(idxdof,idxdof);                                            % [1/H]  :  reduce K to DOFs (assumes triviel Dirichlet BC)
Mmagsh   = Mmag(idxdof,idxdof);                                            % [S]    :  reduce M to DOFs (assumes triviel Dirichlet BC)
Pmagsh   = Pmag(idxdof,:);                                                 % [ ]    :  reduce P to DOFs (assumes triviel Dirichlet BC)

%------------------------------------------------------------------------
% IMPLEMENTATION POINT - DONE!
% compute the system matrix in frequency domain
%------------------------------------------------------------------------
						
Zmagsh=Kmagsh+1i*omega*Mmagsh;                                              % [1/H]  :  complex-valued matrix pencil
% solve problem in frequency domain (direct solver)
ash_phasor=Zmagsh\Pmagsh;                                                  % [Wb]   :  line-integrated magnetic vector potential phasor
% building the whole solution (zeros on idxbdry)
a_phasor         = zeros(length(prb.node),1);                              % [Wb]   :  init solution vector
a_phasor(idxdof)    = ash_phasor;                                         % [Wb]   :  enflate solution

% computing magnetic vector potential (divide by depth) - a_phasor is line-integrated
A_phasor         = savedivide(a_phasor,fem_depth(prb));                   % [Wb/m]  

% computing magnetic flux phasor - B=rot(A)
B_phasor         = fem_curl(prb,a_phasor);                                 % [T]  

% E = - time derivative of magnetic vector potential
E_phasor_nodes         = -1i*omega*A_phasor;                                    % [V/m]  :  compute electric field phasor in the center of each element

% computing the E_phasor of each elem
% taking the mean electric field of the nodes of each elem
E_phasor = zeros(prb.numelem, 1);
for k = 1:prb.numelem
    E_phasor(k) = ( E_phasor_nodes(prb.elem(k,1)) + E_phasor_nodes(prb.elem(k,2)) + E_phasor_nodes(prb.elem(k,3)) ) / 3;
end

% check whether MQS simulation works
errorMqs=norm(a_phasor-prb.node(:,3));
%fprintf('Error in MQS simulation compared to FEMM: %e\n',errorMqs);

% Heat:

% electric conductivity
sigma = prb.regicond(prb.blocklabels(prb.elem(:,4),3));

% boundaries without r=0 (no Dirichlet BC on the symmetrie axis)
idxbdry=bdrycond_getidx(prb,0,0);

% Q_phasor_ges has to be computed in two parts because there shouldnt be induced currents in the coils

% First part of Q_phasor (caused by source-current-density)
Q_phasor1= savedivide(ones(prb.numelem,1),sigma) .* 0.5 .* (abs(J_elem).^2);


% Second part of Q_phasor (caused by induced-current-density)

% Set electric conductivity to zero for each element in the coils (so no induced currents)
for k=1:prb.numelem
    bl=prb.elem(k,4);
    wr=prb.blocklabels(bl,5); 
    if (wr ~= 0)
        sigma(k) = 0;
    end
end

Q_phasor2= sigma .* 0.5 .* (abs(E_phasor).^2);

% compute entire Q_phasor:
Q_phasor_ges=Q_phasor1+Q_phasor2;

% construct matrices of Heat-Problem
%   compute system-matrix
Dheat = feh_divgrad(prb, prb.regiq);
%   compute right-side vector
Fheat = feh_source(prb, Q_phasor_ges);

% set boundaries (Dirichlet BC)
Dheat(idxbdry,:) = 0;

for index=idxbdry'
    Dheat(index,index) = 1;
end
% room temperature: 20°C
Fheat(idxbdry) = 293.15; 

% solve the heat problem
T_phasor = Dheat\Fheat;


% maximum temperatures for each material
T_phasor_elem = zeros(prb.numelem);
max_temperature = zeros(1, prb.numregi);
for k = 1:prb.numelem
	% mean temperature of the nodes of the elem
    T_phasor_elem(k) = ( T_phasor(prb.elem(k,1)) + T_phasor(prb.elem(k,2)) + T_phasor(prb.elem(k,3)) ) / 3;
    if (max_temperature(1, prb.blocklabels(prb.elem(k,4),3)) <= T_phasor_elem(k)) 
        max_temperature(1, prb.blocklabels(prb.elem(k,4),3)) = T_phasor_elem(k);
    end
end

% Return values in the struct prb2:
prb2.max_temp_regions=max_temperature;
prb2.T=T_phasor;
prb2.elem=prb.elem;
prb2.node=prb.node;
prb2.E=E_phasor_nodes;
prb2.A=A_phasor;
prb2.B=B_phasor;
prb2.regions=prb.regilab;
prb2.error=errorMqs;
prb2.Q=Q_phasor_ges;
prb2.J=J_elem;
end
