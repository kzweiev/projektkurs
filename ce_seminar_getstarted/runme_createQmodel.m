% Projektkurs CE 2013
% Institut Theorie Elektromagnetischer Felder, Prof. Dr. Sebastian Schöps
% Martin Lilienthal, M.Sc. – Dipl.-Phys. Fritz Kretzschmar – Klaus Klopfer, M.Sc. – Dipl.-Ing. Uwe Niedermayer
%
% basierend auf der Vorlesungsreihe "Electromagnetic Field Simulation for Electrical Energy Applications"
% von Prof. De Gersem, Aachen
%
% LITERATUR:
%
% Salon J. Sheppard
% Finite Element Analysis Of Electrical Machines
% Springer 2006
%
% Roland W. Lewis, Perumal Nithiarasu, Kankanhalli N. Seetharamu
% Fundamentals of the Finite Element Method for Heat and Fluid Flow
% John Wiley & Sons, 2004
% http://onlinelibrary.wiley.com/book/10.1002/0470014164
%

% get data from FEMM
path='models\einfachesModell_140000hz.ans';
prb      = read_femm(path,0);                                  % []     :  read problem ".ans"-file
prb = regiq(prb);

% paramters
freq     = sscanf(prb.Frequency,'%f');                                     % [Hz]   :  frequency from FEMM
omega    = 2*pi*freq;                                                      % [Hz]   :  angular frequency
%------------------------------------------------------------------------
% IMPLEMENTATION POINT
% choose current - DONE!
%------------------------------------------------------------------------
current  =     prb.CircuitProps(1).TotalAmps_re;                                                                   % [A]    :  get current from FEMM

% construct matrices
Kmag     = fem_curlcurl(prb,prb.regirelu);                                 % [1/H]  :  curl-curl matrix
Mmag     = fem_edgemass(prb,prb.regicond);                                 % [S]    :  matrix of conductivity
[Pmag, J_elem]     = fem_source(prb);                                                % [ ]    :  distribution matrix (for currents)

% set boundary conditions
idxbdry  = bdrycond_getidx(prb,1,1);                                         %
 %[#]    :  get boundary indices
%------------------------------------------------------------------------
% IMPLEMENTATION POINT - DONE!
% compute the indices of degrees of freedom
%------------------------------------------------------------------------
%idxdof;                                                                    % [#]    :  compute indices of degrees of freedom
idxdof=[1:prb.numnode];
for boundaryelem = idxbdry'
	idxdof=idxdof(idxdof~=boundaryelem);
end

Kmagsh   = Kmag(idxdof,idxdof);                                            % [1/H]  :  reduce K to DOFs (assumes triviel Dirichlet BC)
Mmagsh   = Mmag(idxdof,idxdof);                                            % [S]    :  reduce M to DOFs (assumes triviel Dirichlet BC)
Pmagsh   = Pmag(idxdof,:);                                                 % [ ]    :  reduce P to DOFs (assumes triviel Dirichlet BC)

%------------------------------------------------------------------------
% IMPLEMENTATION POINT - DONE!
% compute the system matrix in frequency domain
%------------------------------------------------------------------------
                                 % solve problem in frequency domain

Zmagsh=Kmagsh+1i*omega*Mmagsh;                                              % [1/H]  :  complex-valued matrix pencil
ash_phasor=Zmagsh\Pmagsh;                                                  % [Wb]   :  line-integrated magnetic vector potential phasor
a_phasor         = zeros(length(prb.node),1);                              % [Wb]   :  init solution vector
a_phasor(idxdof)    = ash_phasor;                                         % [Wb]   :  enflate solution
A_phasor         = savedivide(a_phasor,fem_depth(prb));                   % [Wb/m] :  magnetic vector potential (divide by depth)
B_phasor         = fem_curl(prb,a_phasor);                                 % [T]    :  compute magnetic flux phasor
E_phasor_nodes         = -1i*omega*A_phasor;                                    % [V/m]  :  compute electric field phasor in the center of each element
E_phasor = zeros(prb.numelem, 1);
for k = 1:prb.numelem
    E_phasor(k) = ( E_phasor_nodes(prb.elem(k,1)) + E_phasor_nodes(prb.elem(k,2)) + E_phasor_nodes(prb.elem(k,3)) ) / 3;
end

% check whether MQS simulation works
fprintf('Error in MQS simulation compared to FEMM: %e\n',norm(a_phasor-prb.node(:,3)));

% HEAT
sigma = prb.regicond(prb.blocklabels(prb.elem(:,4),3));

idxbdry=bdrycond_getidx(prb,1,0); % boundaries without r=0

J_phasor1 = J_elem;
Q_phasor1= savedivide(ones(prb.numelem,1),sigma) .* 0.5 .* (abs(J_phasor1).^2);
for k=1:prb.numelem
    bl=prb.elem(k,4);
    wr=prb.blocklabels(bl,5); 
    if (wr ~= 0)
        sigma(k) = 0;
    end
end
J_phasor2 =  E_phasor;
Q_phasor2= sigma .* 0.5 .* (abs(J_phasor2).^2);
Q_phasor_ges=Q_phasor1+Q_phasor2;
%Error=(Q_phasor-Q_phasor2);




prb.node(:,1:2) = prb.node(:,1:2) * 100;

openfemm
newdocument(2);
hi_probdef('centimeters','axi',1E-8)

max_x = max(prb.node(:,1));
max_y = max(prb.node(:,2));
min_y = min(prb.node(:,2));

hi_addboundprop('Rand', 0, 293.15, 0, 0, 0, 0);

for k=1:prb.numelem
    regi = prb.blocklabels(prb.elem(k,4),3);
    hi_addmaterial(k,prb.regiq(regi),prb.regiq(regi),Q_phasor_ges(k),0);
    
    x1 = prb.node(prb.elem(k,1),1);
    y1 = prb.node(prb.elem(k,1),2);
    x2 = prb.node(prb.elem(k,2),1);
    y2 = prb.node(prb.elem(k,2),2);
    x3 = prb.node(prb.elem(k,3),1);
    y3 = prb.node(prb.elem(k,3),2);
    mpx = (x1 + x2 + x3) / 3;
    mpy = (y1 + y2 + y3) / 3;
    hi_drawline(x1,y1,x2,y2);
    hi_drawline(x2,y2,x3,y3);
    hi_drawline(x3,y3,x1,y1);
    hi_addblocklabel(mpx,mpy);
    hi_selectlabel(mpx,mpy);
    hi_setblockprop(k,1,0,0);
    hi_clearselected();
    
    % rechter rand
    if (x1 == max_x && x2 == max_x)
        hi_selectsegment((x1+x2)/2,(y1+y2)/2);
        hi_setsegmentprop('Rand', 0, 1, 0, 0, '<None>');
        hi_clearselected();
    end
    if (x2 == max_x && x3 == max_x)
        hi_selectsegment((x2+x3)/2,(y2+y3)/2);
        hi_setsegmentprop('Rand', 0, 1, 0, 0, '<None>');
        hi_clearselected();
    end
    if (x3 == max_x && x1 == max_x)
        hi_selectsegment((x3+x1)/2,(y3+y1)/2);
        hi_setsegmentprop('Rand', 0, 1, 0, 0, '<None>');
        hi_clearselected();
    end
    % oberer rand
    if (y1 == max_y && y2 == max_y)
        hi_selectsegment((x1+x2)/2,(y1+y2)/2);
        hi_setsegmentprop('Rand', 0, 1, 0, 0, '<None>');
        hi_clearselected();
    end
    if (y2 == max_y && y3 == max_y)
        hi_selectsegment((x2+x3)/2,(y2+y3)/2);
        hi_setsegmentprop('Rand', 0, 1, 0, 0, '<None>');
        hi_clearselected();
    end
    if (y3 == max_y && y1 == max_y)
        hi_selectsegment((x3+x1)/2,(y3+y1)/2);
        hi_setsegmentprop('Rand', 0, 1, 0, 0, '<None>');
        hi_clearselected();
    end
    % unterer rand
    if (y1 == min_y && y2 == min_y)
        hi_selectsegment((x1+x2)/2,(y1+y2)/2);
        hi_setsegmentprop('Rand', 0, 1, 0, 0, '<None>');
        hi_clearselected();
    end
    if (y2 == min_y && y3 == min_y)
        hi_selectsegment((x2+x3)/2,(y2+y3)/2);
        hi_setsegmentprop('Rand', 0, 1, 0, 0, '<None>');
        hi_clearselected();
    end
    if (y3 == min_y && y1 == min_y)
        hi_selectsegment((x3+x1)/2,(y3+y1)/2);
        hi_setsegmentprop('Rand', 0, 1, 0, 0, '<None>');
        hi_clearselected();
    end
    
end

hi_zoomnatural;
hi_saveas('models\femm_modell_Qmodel.FEH');
%hi_createmesh();
%hi_analyze(1) % 0 is for visible window

