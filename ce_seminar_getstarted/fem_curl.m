function B=fem_curl(prb,a)

% function B=curl(prb,a)
% computes the magnetic flux density for a given distribution of the line-integrated magnetic vector potential
%
% input parameters
%       prb       :      : FEMM problem data structure
%       a         : [Wb] : line-integrated magnetic vector potential, numnode-by-1 vector
%
% output parameters
%       B         : [T]  : magnetic  flux density, numelem-by-2 vector
%
% see also GRAD, DIV

[NOTa,b,c,area,r,z]=linear_shape_functions(prb);
u = [a(prb.elem(:,1)) a(prb.elem(:,2)) a(prb.elem(:,3))];
if strcmp(prb.ProblemType,'axisymmetric')
  denom=-(2*area).*(2*pi*mean(r,2));
else
  denom=2*area*prb.Depth;
end

%------------------------------------------------------------------------
% IMPLEMENTATION POINT -DONE!
% compute the element-wise magnetic flux density
%------------------------------------------------------------------------
B = zeros(prb.numelem, 2);

for k = 1:prb.numelem
    B(k,1) = u(k,:) * c(k,:)' / denom(k);
    B(k,2) = - u(k,:) * b(k,:)' / denom(k);
end
