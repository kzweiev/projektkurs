function auswertung (prb, a, A, B, E, J, T, filename)

% Skript zur Auswertung
% auswertung(prb,a_phasor,A_phasor,B_phasor,E_phasor,J_phasor,T_phasor,name)


% Example:
% auswertung(prb,a_phasor,A_phasor,B_phasor,E_phasor,J_phasor,T_phasor,'FemmModell2')
%ourprb=prb;
%ourprb.node(:,3)=A;
%save_femm(ourprb,fopen(['results/' filename '_our.ans'],'w+'));
%fclose('all');
%save_vtk (prb,['results/' filename '_our.vtu'],{'A_phasor',A;'T_phasor',T}, {'B_phasor  ',B;'E_phasor',E;'J_phasor  ',J});
h=figure;
subplot(3,1,1)
plot(abs(prb.node(:,3)),'red')
subplot(3,1,2)
plot(abs(a),'blue')
subplot(3,1,3)
plot(abs(prb.node(:,3))-abs(a),'green')
saveas(h,['results/' filename '_a_phasor'],'png')