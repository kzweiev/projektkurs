function prb=regiq(prb)
% gibt ergänzte prb Struktur mit regiq zurück
% %erstellt eine 6x2 Matrix, so wie prb.regirelu nur mit
% %Wärmeleitkoeffizienten

regiq = zeros(prb.numregi,2);

for k = 1:prb.numregi
    switch(cell2mat(prb.regilab(k)))
        case 'Luft'
            regiq(k,1) = 0.0261;
        case 'Aluminium'
            regiq(k,1) = 236;
        case 'Ferrit'
            regiq(k,1) = 5;
        case 'Kupfer'
            regiq(k,1) = 401;    
        case 'Asphalt'
            regiq(k,1) = 0.03;
        case 'Kunststoff'
            regiq(k,1) = 0.03;
        case 'Messing'
            regiq(k,1) = 120;
        case 'Eisen' 
            regiq(k,1) = 80.2;
        otherwise
            regiq(k,1) = 0;
    end 
end

%isotrop material
%[W/(m*K)]
regiq(:,2) = regiq(:,1);
prb.regiq = regiq;
