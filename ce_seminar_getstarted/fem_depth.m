function depth=fem_depth(prb)

% function depth=FEM_DEPTH(prb)
% computes the primal edge length, i.e., the depth in the planar case
%
% input parameters
%    prb        : FEMM problem data structure
%
% output parameters
%    depth      : [m] : depth
%

r = prb.node(:,1);

if strcmp(prb.ProblemType,'axisymmetric')
  depth=2*pi*r;
else
  depth=prb.Depth*ones(prb.numnode,1);
end
