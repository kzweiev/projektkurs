% Auswertung der Heat-Komponente
path='models\heat_val.anh';
prb      = read_femm_feh(path,0);                                  % []     :  read problem ".ans"-file
prb = regiq(prb);


% HEAT
Dheat = feh_divgrad(prb, 10);
Fheat = feh_source(prb, zeros(prb.numelem,1));

% set boundaries
idxbdry=bdrycond_getidx(prb,0,1);   
Dheat(idxbdry,:) = 0;
for index=idxbdry'
    Dheat(index,index) = 1;
end

idxbdry=bdrycond_getidx(prb,0,2); %Top
Fheat(idxbdry) = 500; % Boundary Top 500K

idxbdry=bdrycond_getidx(prb,0,3); %Rest
Fheat(idxbdry) = 100; % Boundary Rest 100K

T_phasor = Dheat\Fheat;


x=prb.node(:,1);
y=prb.node(:,2);
%x=[0:1000]/1000
%y=ones(1,1001)
T_ana=0;    
    for n=1:200
        
        delta=  (((-1)^(n+1))+1)/n.*sin (n*pi*x).*(sinh(n*pi*y)./ sinh (n*pi));    
        T_ana=T_ana+delta;    
    end
T_ana=((800/pi).*T_ana)+100;

max(T_ana)
%trimesh(prb.elem(:,1:3), prb.node(:,1), prb.node(:,2),T_ana)
%view(2)

%Analytic Solution
% x=prb.node(:,1);
% y=prb.node(:,2);
% x=0.5;
% y=1;
% T_num=0;
% for i=1:200
%     T_ana=0;    
%     for n=1:i
%         delta=  (((-1)^(n+1))+1)/n*sin (n*pi*x).*(sinh(n*pi*y)./ sinh (n*pi));    
%         T_ana=T_ana+delta;    
%     end
%     T_ana=((800/pi)*T_ana)+100;
%     T_num(i,1)=T_ana;
% end
% x=1;
% y=0.5;
% for i=1:200
%     T_ana=0;
%     for n=1:i
%         delta=  (((-1)^(n+1))+1)/n*sin (n*pi*x).*(sinh(n*pi*y)./ sinh (n*pi));    
%         T_ana=T_ana+delta;    
%     end
%     T_ana=((800/pi)*T_ana)+100;
%     T_num(i,2)=T_ana;
% end
% 
% x=0.5;
% y=0.9;
% for i=1:200
%     T_ana=0;
%     for n=1:i
%         delta=  (((-1)^(n+1))+1)/n*sin (n*pi*x).*(sinh(n*pi*y)./ sinh (n*pi));    
%         T_ana=T_ana+delta;    
%     end
%     T_ana=((800/pi)*T_ana)+100;
%     T_num(i,3)=T_ana;
% end
% 
% x=0.994109375000000
% y=1;
% for i=1:200
%     T_ana=0;
%     for n=1:i
%         delta=  (((-1)^(n+1))+1)/n*sin (n*pi*x).*(sinh(n*pi*y)./ sinh (n*pi));    
%         T_ana=T_ana+delta;    
%     end
%     T_ana=((800/pi)*T_ana)+100;
%     T_num(i,5)=T_ana;
% end
% 
% 
% x=0.5;
% y=0.995;
% for i=1:200
%     T_ana=0;
%     for n=1:i
%         delta=  (((-1)^(n+1))+1)/n*sin (n*pi*x).*(sinh(n*pi*y)./ sinh (n*pi));    
%         T_ana=T_ana+delta;    
%     end
%     T_ana=((800/pi)*T_ana)+100;
%     T_num(i,4)=T_ana;
% end

% 
x=prb.node(:,1);
y=prb.node(:,2);
    T_ana=0;
    for n=1:i
        delta=  (((-1)^(n+1))+1)/n*sin (n*pi*x).*(sinh(n*pi*y)./ sinh (n*pi));    
        T_ana=T_ana+delta;    
    end
    T_ana=((800/pi)*T_ana)+100;

% subplot(1,5,1)
% 
% plot(T_num(:,2))
% xlabel('n')
% ylabel('Temp [�C]')
% title('x=1, y=0.5')
% xlim([0 200])
% axis tight
% 
% 
% subplot(1,5,2)
% 
% plot(T_num(:,3))
% xlabel('n')
% ylabel('Temp [�C]')
% title('x=0.5, y=0.9')
% xlim([0 200])
% 
% subplot(1,5,3)
% plot(T_num(:,4))
% xlabel('n')
% ylabel('Temp [�C]')
% title('x=0.5, y=0.995')
% xlim([0 200])
% 
% 
% subplot(1,5,4)
% plot(T_num(:,1))
% xlabel('n')
% ylabel('Temp [�C]')
% title('x=0.5, y=1')
% xlim([0 200])
% 
% subplot(1,5,5)
% plot(T_num(:,5))
% xlabel('n')
% ylabel('Temp [�C]')
% title('x=0.9941, y=0.1')
% xlim([0 200])


%TEMPVERTEILUNG AUSWERTUNG
max(T_ana);
subplot(1,2,2)
trisurf(prb.elem(:,1:3), prb.node(:,1), prb.node(:,2),T_ana,'LineStyle','none')
view(2)
caxis([100 560])
xlabel('x[m]')
ylabel('y[m]')
title('Temperaturverteilung, Analytische L�sung, n=200')
% 
% subplot(1,2,1)
% trisurf(prb.elem(:,1:3), prb.node(:,1), prb.node(:,2),T_phasor,'LineStyle','none')
% caxis([100 560])
% view(2)
% xlabel('x[m]')
% ylabel('y[m]')
% title('Temperaturverteilung, Eigene L�sung')

%ERROR AUSWERTUNG

% trisurf(prb.elem(:,1:3), prb.node(:,1), prb.node(:,2),(T_phasor-T_ana)./(T_phasor./100),'LineStyle','none')
% colormap(cmap)
% caxis([-0.1 0.1])
% view(2)
% colorbar
% xlabel('x[m]')
% ylabel('y[m]')
% title('Temperaturverteilung, relativer Fehler[%]')


% subplot(1,3,3)
% trimesh(prb.elem(:,1:3), prb.node(:,1), prb.node(:,2),log (abs((T_phasor-T_ana))))
% view(2)

