function [Pstr, J_elem]=fem_source(prb)

% function Pstr=FEM_SOURCE(prb)
% computes the coupling blocks for stranded conductors
%
% input parameters
%    prb        : FEMM problem data structure
%
% output parameters
%    Pstr       : []   : coupling blocks for stranded conductors
%
% see also DIVGRAD, CURLCURL, FEH_SOURCE

[a,b,c,area,r,z]=linear_shape_functions(prb);

% computing the area of each block
for bl=1:size(prb.blocklabels,1)
  blockarea(bl)=sum(area(find(prb.elem(:,4)==bl)));
end

Pstr=zeros(prb.numnode,1);
% J_elem for heat-part
J_elem = zeros(prb.numelem,1);
for k=1:prb.numelem
  bl=prb.elem(k,4);
  wr=prb.blocklabels(bl,5);                                                % [@]   : index of circuit
  Nt=prb.blocklabels(bl,8);                                                % [@]   : number of turns
  if wr~=0
      
    idx=prb.elem(k,1:3);
    %------------------------------------------------------------------------
    % IMPLEMENTATION POINT - DONE
    % compute the elementary current-load vector
    %------------------------------------------------------------------------

	% str2num because prb.CircuitProps(wr).TotalAmps_re returns string 
	% Stromdichte constant for each block: current divided by blockarea
	Stromdichte=str2num(prb.CircuitProps(wr).TotalAmps_re) * Nt / blockarea(bl);
	
	Pstr(idx)=Pstr(idx)+(1/3)*Stromdichte*area(k)*ones(3,1);
	
	%For heat-part:
    J_elem(k) = Stromdichte;
  end
end
