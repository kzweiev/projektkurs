function prb=read_femm_feh(femmfile,plotflag)

% function prb=READ_FEMM(fin)
% reads a FEMM-formatted .fem or .ans file into a FE-prb structure
%
% input parameters
%     femmfile       : file pointer or filename
%     plotflag       : 1/0 : (optional; default: 0)
%
% output parameters
%     prb            : FE-problem structure
%

% A. Parameter control
if exist(femmfile,'file')
    fin = fopen(femmfile);
else
    fin = femmfile;
end
if nargin<2
  plotflag=0;
end

% B. Read data from file
while ~feof(fin)
  ln=fgetl(fin);
  [first,ln]=strtok(ln);
  if ~isempty(first)
    [second,ln]=strtok(ln);
    [third,ln]=strtok(ln);
    if (first(1)=='[') && (first(end)==']')
      first=first(2:end-1);
    end
    if second=='='
      prb.(first)=third;
    end
    switch first
      case {'Depth'}                 % single double number
        prb.(first)=sscanf(prb.(first),'%f');
      case {'BdryProps','BlockProps','ConductorProps'}
        num=sscanf(prb.(first),'%d');
        prb=rmfield(prb,first);
        for p=1:num
          % D.1. Name
          [subfirst,ln]=strtok(fgetl(fin));    % <BeginBdry>
          if (subfirst(1)~='<') || (subfirst(end)~='>')
            error('sth wrong1');
          end
          if (subfirst(2:6)~='Begin')
            error('sth wrong2');
          end
          propname=subfirst(7:end-1);
          [subfirst,ln]=strtok(fgetl(fin));
          while ~strcmp(subfirst,['<End' propname '>'])
            [subsecond,ln]=strtok(ln);
            [subthird,ln]=strtok(ln);
            if (subfirst(1)~='<') || (subfirst(end)~='>')
              error('sth wrong3');
            end
            subfirst=subfirst(2:end-1);
            if subsecond~='='
              error('sth wrong4');
            end
            prb.(first)(p).(subfirst)=subthird;
            [subfirst,ln]=strtok(fgetl(fin));
          end
        end
      case 'NumPoints'
        numpoints=sscanf(prb.NumPoints,'%d');
        prb=rmfield(prb,'NumPoints');
        prb.points=fscanf(fin,'%f',[4 numpoints])';
      case 'NumSegments'
        numsegments=sscanf(prb.NumSegments,'%d');
        prb=rmfield(prb,'NumSegments');
        prb.segments=fscanf(fin,'%f',[6 numsegments])';
      case 'NumArcSegments'
        numarcsegments=sscanf(prb.NumArcSegments,'%d');
        prb=rmfield(prb,'NumArcSegments');
        prb.arcsegments=fscanf(fin,'%f',[7 numarcsegments])';
      case 'NumHoles'
        numholes=sscanf(prb.NumHoles,'%d');
        prb.holes=fscanf(fin,'%f',[3 numholes])';
      case 'NumBlockLabels'
        numblocklabels=sscanf(prb.NumBlockLabels,'%d');
        prb=rmfield(prb,'NumBlockLabels');
        prb.blocklabels=fscanf(fin,'%f',[6 numblocklabels])';
      case 'Solution'
        numnode=fscanf(fin,'%d',1);
        ln=fgetl(fin);                      % discard the line termination
        ln=fgetl(fin);                      % read the first line to check whether a real-valued or a complex-valued solution is given
        [values,num]=sscanf(ln,'%f');
        values=[values'; fscanf(fin,'%f',[num numnode-1])'];
        switch num
          case 3
            prb.node=values;
          case 4
            prb.node=[values(:,1:2) values(:,3)+sqrt(-1)*values(:,4)];
          otherwise
            error('format problem encountered in read_femm.m');
        end
        numelem=fscanf(fin,'%d',1);
        prb.elem=fscanf(fin,'%d',[4 numelem])';
        numwhatisthis=fscanf(fin,'%d',1);
        prb.whatisthis=fscanf(fin,'%d',[2 numwhatisthis])';        
    end
  end
end

if exist(femmfile,'file')
    fclose(fin);
end

% C. Adapt indices
prb.elem=prb.elem+1;
prb.segments(:,1:2)=prb.segments(:,1:2)+1;
prb.arcsegments(:,1:2)=prb.arcsegments(:,1:2)+1;
prb.numnode=size(prb.node,1);                                              % [#]    : number of mesh nodes
prb.numelem=size(prb.elem,1);                                              % [#]    : number of mesh elements
prb.idxelem=prb.blocklabels(prb.elem(:,4),3);                              % indices of the regions

% D. Scale lengths
switch prb.LengthUnits
  case 'meters'
    lfac=1;
  case 'centimeters'
    lfac=100;
  case 'millimeters'
    lfac=1000;
  otherwise
    error('unit %s not known\n',prb.LengthUnits);
end
prb.points(:,1:2)=prb.points(:,1:2)/lfac;
prb.blocklabels(:,1:2)=prb.blocklabels(:,1:2)/lfac;
prb.Depth=prb.Depth/lfac;
prb.node(:,1:2)=prb.node(:,1:2)/lfac;

% % E. Adapt solution
% if ~strcmp(prb.ProblemType,'axisymmetric')      % FEMM stores Az
%   prb.node(:,3)=prb.node(:,3)*prb.Depth;
% else                                            % FEMM stores 2*pi*r*Atheta
%   % do nothing
% end

% F. Read and modify material and excitation properties
prb.numregi=length(prb.BlockProps);                                        % [#]    : number of regions/domains/blocks
for rg=1:prb.numregi
  prb.regilab{rg}=prb.BlockProps(rg).BlockName(2:end-1);                   % []     : region name
  prb.regitherm_cond(rg,1:2)=[sscanf(prb.BlockProps(rg).Kx,'%f') ...
    sscanf(prb.BlockProps(rg).Ky,'%f')];                                   % [W/mK] : thermal conductivity
prb.regiqv(rg,1)=sscanf(prb.BlockProps(rg).qv,'%f');                       % [W/m^3]  : volume heat generation
end
prb.numblock=size(prb.blocklabels,1);


% G. Find the boundary nodes
prb.idxdir=find(prb.node(:,3)==0);                                         % []     : indices of the Dirichlet constraints (HDG: dangerous)
prb.idxdof=setdiff(1:prb.numnode,prb.idxdir);                              % []     : indices of the degrees of freedom
prb.numdof=length(prb.idxdof);                                             % [#]    : number of degrees of freedom


% I. Extend the FEMM data structures
% prb=bdrycond_initialise(prb);                                              %        : initialise the prb.bdrycond data structure

% J. Plot
if plotflag                                                   %        : plot the nodes with Dirichlet constraints
  figure(1); clf; trimesh(prb.elem(:,1:3),prb.node(:,1),prb.node(:,2));
  axis equal; axis off; xlabel('x (m)'); ylabel('y (m)'); title('mesh');
end

