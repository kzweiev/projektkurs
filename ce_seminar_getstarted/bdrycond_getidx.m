function idxbdry=bdrycond_getidx(prb,plotflag,bdryMode)

% function prb=BDRYCOND_GETIDX(prb,plotflag)
% get the indices of nodes at the boundary
%
% input parameters
%    prb        : FEMM problem data structure
%    plotflag   : 1/0 : (optional; default: 0)
%    bdryMode   : 3/2/1/0 : (optional; default: 0)
%                  0: top, bottom, right side
%                  1: all sides
%                  2: top side
%                  3: left, bottom, right side
%
% output parameters
%    idxbdry    : indices of boundary nodes
%

% we assume a rectangular domain
% nodes at the boundary <=> one coordinate is max or min
% we DONT ignore the symmetry axis min(prb.node(:,1))

%------------------------------------------------------------------------
% IMPLEMENTATION POINT
% compute the indices of the necessary boundary nodes - DONE!
%------------------------------------------------------------------------
maxr=max(prb.node(:,1));
minr=min(prb.node(:,1));
maxz=max(prb.node(:,2));
minz=min(prb.node(:,2));

switch bdryMode
    case 3
    bdrynodes= [find(prb.node(:,1)==maxr); find(prb.node(:,1)==minr); find(prb.node(:,2)==minz);];
    case 2   
    bdrynodes=  find(prb.node(:,2)==maxz);
    case 1    
    bdrynodes= [find(prb.node(:,1)==maxr); find(prb.node(:,1)==minr); find(prb.node(:,2)==maxz); find(prb.node(:,2)==minz);];
    case 0   
    bdrynodes= [find(prb.node(:,1)==maxr); find(prb.node(:,2)==maxz); find(prb.node(:,2)==minz);];
end

% count each node only once and sort them 
idxbdry = sort(unique(bdrynodes));

% plot the nodes with Dirichlet constraints
if plotflag                                                   
  figure(1); clf; 
  trimesh(prb.elem(:,1:3),prb.node(:,1),prb.node(:,2),'Color','blue');
  hold on;
  plot(prb.node(idxbdry,1),prb.node(idxbdry,2),'.','Color','red');
  axis equal; axis off; xlabel('x (m)'); ylabel('y (m)'); title('mesh');
end
end


