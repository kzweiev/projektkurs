% Projektkurs CE 2013
% Institut Theorie Elektromagnetischer Felder, Prof. Dr. Sebastian Schöps
% Martin Lilienthal, M.Sc. – Dipl.-Phys. Fritz Kretzschmar – Klaus Klopfer, M.Sc. – Dipl.-Ing. Uwe Niedermayer
%
% basierend auf der Vorlesungsreihe "Electromagnetic Field Simulation for Electrical Energy Applications"
% von Prof. De Gersem, Aachen
%
% LITERATUR:
%
% Salon J. Sheppard
% Finite Element Analysis Of Electrical Machines
% Springer 2006
%
% Roland W. Lewis, Perumal Nithiarasu, Kankanhalli N. Seetharamu
% Fundamentals of the Finite Element Method for Heat and Fluid Flow
% John Wiley & Sons, 2004
% http://onlinelibrary.wiley.com/book/10.1002/0470014164
%

% get data from FEMM
path='models\femm_modell.ans';
prb      = read_femm(path,0);                                  % []     :  read problem ".ans"-file
prb = regiq(prb);

% paramters
freq     = sscanf(prb.Frequency,'%f');                                     % [Hz]   :  frequency from FEMM
omega    = 2*pi*freq;                                                      % [Hz]   :  angular frequency
%------------------------------------------------------------------------
% IMPLEMENTATION POINT
% choose current - DONE!
%------------------------------------------------------------------------
current  =     prb.CircuitProps(1).TotalAmps_re;                                                                   % [A]    :  get current from FEMM

% construct matrices
Kmag     = fem_curlcurl(prb,prb.regirelu);                                 % [1/H]  :  curl-curl matrix
Mmag     = fem_edgemass(prb,prb.regicond);                                 % [S]    :  matrix of conductivity
[Pmag, J_elem]     = fem_source(prb);                                                % [ ]    :  distribution matrix (for currents)

% set boundary conditions
idxbdry  = bdrycond_getidx(prb,1,1);                                         %
 %[#]    :  get boundary indices
%------------------------------------------------------------------------
% IMPLEMENTATION POINT - DONE!
% compute the indices of degrees of freedom
%------------------------------------------------------------------------
%idxdof;                                                                    % [#]    :  compute indices of degrees of freedom
idxdof=[1:prb.numnode];
for boundaryelem = idxbdry'
	idxdof=idxdof(idxdof~=boundaryelem);
end

Kmagsh   = Kmag(idxdof,idxdof);                                            % [1/H]  :  reduce K to DOFs (assumes triviel Dirichlet BC)
Mmagsh   = Mmag(idxdof,idxdof);                                            % [S]    :  reduce M to DOFs (assumes triviel Dirichlet BC)
Pmagsh   = Pmag(idxdof,:);                                                 % [ ]    :  reduce P to DOFs (assumes triviel Dirichlet BC)

%------------------------------------------------------------------------
% IMPLEMENTATION POINT - DONE!
% compute the system matrix in frequency domain
%------------------------------------------------------------------------
                                 % solve problem in frequency domain

Zmagsh=Kmagsh+1i*omega*Mmagsh;                                              % [1/H]  :  complex-valued matrix pencil
ash_phasor=Zmagsh\Pmagsh;                                                  % [Wb]   :  line-integrated magnetic vector potential phasor
a_phasor         = zeros(length(prb.node),1);                              % [Wb]   :  init solution vector
a_phasor(idxdof)    = ash_phasor;                                         % [Wb]   :  enflate solution
A_phasor         = savedivide(a_phasor,fem_depth(prb));                   % [Wb/m] :  magnetic vector potential (divide by depth)
B_phasor         = fem_curl(prb,a_phasor);                                 % [T]    :  compute magnetic flux phasor
E_phasor_nodes         = -1i*omega*A_phasor;                                    % [V/m]  :  compute electric field phasor in the center of each element
E_phasor = zeros(prb.numelem, 1);
for k = 1:prb.numelem
    E_phasor(k) = ( E_phasor_nodes(prb.elem(k,1)) + E_phasor_nodes(prb.elem(k,2)) + E_phasor_nodes(prb.elem(k,3)) ) / 3;
end

% check whether MQS simulation works
fprintf('Error in MQS simulation compared to FEMM: %e\n',norm(a_phasor-prb.node(:,3)));

% HEAT
sigma = prb.regicond(prb.blocklabels(prb.elem(:,4),3));

idxbdry=bdrycond_getidx(prb,1,0); % boundaries without r=0

J_phasor1 = J_elem;

Q_phasor1= savedivide(ones(prb.numelem,1),sigma) .* 0.5 .* (abs(J_phasor1).^2);
for k=1:prb.numelem
    bl=prb.elem(k,4);
    wr=prb.blocklabels(bl,5); 
    if (wr ~= 0)
        sigma(k) = 0;
    end
 end
% J_phasor2 =  E_phasor;
% Q_phasor2= sigma .* 0.5 .* (abs(J_phasor2).^2);
Q_phasor_ges=Q_phasor1;%+Q_phasor2;
%Error=(Q_phasor-Q_phasor2);
% Matrizen
Dheat = feh_divgrad(prb, prb.regiq);
Fheat = feh_source(prb, Q_phasor_ges);

% set boundaries
Dheat(idxbdry,:) = 0;
for index=idxbdry'
    Dheat(index,index) = 1;
end
Fheat(idxbdry) = 293.15; % zimmertemperatur

T_phasor = Dheat\Fheat;
%reszult femm real(prb.node(:,3))
% maximum temperatures for each material
T_phasor_elem = zeros(prb.numelem);
max_temperature = zeros(1, prb.numregi);
for k = 1:prb.numelem
    T_phasor_elem(k) = ( T_phasor(prb.elem(k,1)) + T_phasor(prb.elem(k,2)) + T_phasor(prb.elem(k,3)) ) / 3;
    if (max_temperature(1, prb.blocklabels(prb.elem(k,4),3)) <= T_phasor_elem(k)) 
        max_temperature(1, prb.blocklabels(prb.elem(k,4),3)) = T_phasor_elem(k);
    end
end

path='models\heat_val_q.anh';
prb2      = read_femm_feh(path,0);


subplot(1,2,1)
trisurf(prb.elem(:,1:3), prb.node(:,1), prb.node(:,2),T_phasor,'LineStyle','none')
view(2)
caxis([293.1 293.6])
xlabel('x[m]')
ylabel('y[m]')
title('Temperaturverteilung, eigene L�sung, Modell ohne E-Feld [K]')



subplot(1,2,2)
trisurf(prb2.elem(:,1:3), prb2.node(:,1), prb2.node(:,2), real(prb2.node(:,3)),'LineStyle','none')
view(2)
caxis([293.1 293.6])
xlabel('x[m]')
ylabel('y[m]')
title('Temperaturverteilung, FEMM, Modell ohne E-Feld [K]')
















