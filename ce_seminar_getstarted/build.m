% builds automatically femm-model with input parameters

function [] = build(metallKoerper, Abstand , Size )
%% metallKoerper int:  0 kein K�rper, 1 Ferritring, 2 Messingring
%% Abstand sinnvoll zwischen 19 und 30
%% size: L�nge in r-Richtung min=70

if nargin==0
    metallKoerper=0;
    Abstand=27; % default
    Size=100;
end

if nargin==1
    Abstand=27; % default
    Size=100;
end

if nargin==2
    Size=100;
end

if Size<70
    Size=70;
end

if Abstand<19 || Abstand>30
    Abstand=27;
end

openfemm
newdocument(0);
mi_probdef(140000,'centimeters','axi',1E-8,'0.0100',30,'0')

mi_addmaterial('Luft',0.01,0.01);
mi_addmaterial('Aluminium',1,1,0,0,59);
mi_addmaterial('Ferrit',15000,15000,0,0,10);
mi_addmaterial('Kupfer',1,1,0,0,58);
mi_addmaterial('Asphalt',1,1);
mi_addmaterial('Kunststoff',1,1);



rRand=[0,Size,Size,0];
zRand=[0,0,40,40];
mi_addnode(rRand,zRand)

rKunststoff1=[0,50,50,0];
zKunststoff1=[7,7,10,10];
mi_addnode(rKunststoff1,zKunststoff1)

rKunststoff2=[0,40,40,0];
zKunststoff2=[25,25,9+Abstand,9+Abstand];
mi_addnode(rKunststoff2,zKunststoff2)

rSpule1=[20,30,30,20];
zSpule1=[7,7,8,8];
mi_addnode(rSpule1,zSpule1)

rSpule2=rSpule1;
zSpule2=zSpule1+ones(1,4)*Abstand;
mi_addnode(rSpule1,zSpule1)

rFerrit1=[0,50,50,0];
zFerrit1=[6,6,7,7];
mi_addnode(rFerrit1,zFerrit1)

rFerrit2=[0,40,40,0];
zFerrit2=zFerrit1+ones(1,4)*(Abstand+2);
mi_addnode(rSpule1,zSpule1)

rAlu=[0,70,70,0];
zAlu=[9+Abstand,9+Abstand,40,40];
mi_addnode(rAlu,zAlu)

for i=1:4;
    if i==4
        j=1;
    else
        j=i+1;
    end
mi_drawline(rRand(i),zRand(i),rRand(j),zRand(j))
mi_drawline(rKunststoff1(i),zKunststoff1(i),rKunststoff1(j),zKunststoff1(j))
mi_drawline(rKunststoff2(i),zKunststoff2(i),rKunststoff2(j),zKunststoff2(j))
mi_drawline(rSpule1(i),zSpule1(i),rSpule1(j),zSpule1(j))
mi_drawline(rSpule2(i),zSpule2(i),rSpule2(j),zSpule2(j))
mi_drawline(rFerrit1(i),zFerrit1(i),rFerrit1(j),zFerrit1(j))
mi_drawline(rFerrit2(i),zFerrit2(i),rFerrit2(j),zFerrit2(j))
mi_drawline(rAlu(i),zAlu(i),rAlu(j),zAlu(j))
end

mi_drawline(rKunststoff1(3),zKunststoff1(3),60,0);

mi_addcircprop('Windung1',9,1)
mi_addcircprop('Windung2',9,1)

mi_addblocklabel((rSpule1(1)+rSpule1(2))/2,(zSpule1(1)+zSpule1(3))/2);
mi_selectlabel((rSpule1(1)+rSpule1(2))/2,(zSpule1(1)+zSpule1(3))/2);
mi_setblockprop('Kupfer',0,0.2,'Windung1',0,0,14)
mi_clearselected

mi_addblocklabel((rSpule2(1)+rSpule2(2))/2,(zSpule2(1)+zSpule2(3))/2);
mi_selectlabel((rSpule2(1)+rSpule2(2))/2,(zSpule2(1)+zSpule2(3))/2);
mi_setblockprop('Kupfer',0,0.2,'Windung2',0,0,9)
mi_clearselected

mi_addblocklabel((rAlu(1)+rAlu(2))/2,(zAlu(1)+zAlu(3))/2);
mi_selectlabel((rAlu(1)+rAlu(2))/2,(zAlu(1)+zAlu(3))/2);
mi_setblockprop('Aluminium',0,0,'',0,0,0)
mi_clearselected

mi_addblocklabel((rFerrit1(1)+rFerrit1(2))/2,(zFerrit1(1)+zFerrit1(3))/2);
mi_selectlabel((rFerrit1(1)+rFerrit1(2))/2,(zFerrit1(1)+zFerrit1(3))/2);
mi_setblockprop('Ferrit',0,0,'',0,0,0)
mi_clearselected

mi_addblocklabel((rFerrit2(1)+rFerrit2(2))/2,(zFerrit2(1)+zFerrit2(3))/2);
mi_selectlabel((rFerrit2(1)+rFerrit2(2))/2,(zFerrit2(1)+zFerrit2(3))/2);
mi_setblockprop('Ferrit',0,0,'',0,0,0)
mi_clearselected

mi_addblocklabel(5,(zKunststoff1(1)+zKunststoff1(3))/2);
mi_selectlabel(5,(zKunststoff1(1)+zKunststoff1(3))/2);
mi_setblockprop('Kunststoff',0,0,'',0,0,0)
mi_clearselected

mi_addblocklabel(5,(zKunststoff2(1)+zKunststoff2(3))/2);
mi_selectlabel(5,(zKunststoff2(1)+zKunststoff2(3))/2);
mi_setblockprop('Kunststoff',0,0,'',0,0,0)
mi_clearselected

mi_addblocklabel(1,1);
mi_selectlabel(1,1);
mi_setblockprop('Asphalt',0,0,'',0,0,0)
mi_clearselected

mi_addblocklabel(90,20);
mi_selectlabel(90,20);
mi_setblockprop('Luft',0,0,'',0,0,0)
mi_clearselected

if metallKoerper==1
rMuenze=[20,20,30,30];
zMuenze=[10,12,12,10];
mi_addnode(rMuenze,zMuenze)
for i=1:4;
    if i==4
        j=1;
    else
        j=i+1;
    end
mi_drawline(rMuenze(i),zMuenze(i),rMuenze(j),zMuenze(j))
mi_addblocklabel((rMuenze(1)+rMuenze(3))/2,(zMuenze(1)+zMuenze(2))/2);
mi_selectlabel((rMuenze(1)+rMuenze(3))/2,(zMuenze(1)+zMuenze(2))/2 );
mi_setblockprop('Ferrit',0,0.2,'',0,0,0)
mi_clearselected
end
end

if metallKoerper==2
    mi_addmaterial('Messing',1,1,0,0,15.5);
rMuenze=[20,20];
zMuenze=[15,20];
mi_addnode(rMuenze,zMuenze)
mi_drawarc(rMuenze(1),zMuenze(1),rMuenze(2),zMuenze(2),180,1);
mi_drawarc(rMuenze(2),zMuenze(2),rMuenze(1),zMuenze(1),180,1);
mi_addblocklabel((rMuenze(1)+rMuenze(2))/2,(zMuenze(1)+zMuenze(2))/2);
mi_selectlabel((rMuenze(1)+rMuenze(2))/2,(zMuenze(1)+zMuenze(2))/2 );
mi_setblockprop('Messing',0,0.2,'',0,0,0)
mi_clearselected
end

if metallKoerper==3
    mi_addmaterial('Eisen',10000,10000,0,0,10);
rMuenze=[0,0,5,5];
zMuenze=[15,16,16,15];
mi_addnode(rMuenze,zMuenze)
for i=1:4;
    if i==4
        j=1;
    else
        j=i+1;
    end
mi_drawline(rMuenze(i),zMuenze(i),rMuenze(j),zMuenze(j))
mi_addblocklabel((rMuenze(1)+rMuenze(3))/2,(zMuenze(1)+zMuenze(2))/2);
mi_selectlabel((rMuenze(1)+rMuenze(3))/2,(zMuenze(1)+zMuenze(2))/2 );
mi_setblockprop('Eisen',0,0.2,'',0,0,0)
mi_clearselected
end
end


mi_addboundprop('Rand',0,0,0,0,0,0,0,0,0);

mi_selectsegment(20,40);
mi_selectsegment(90,40);
mi_selectsegment(100,20);
mi_selectsegment(1,0);
mi_selectsegment(99,0);
mi_setsegmentprop('Rand',0,0,1,0);
mi_clearselected

mi_zoomnatural;
mi_saveas('models\femm_modell2.FEM'); 
mi_createmesh;
mi_analyze(1); % 0 is for visible window
%closefemm;
end