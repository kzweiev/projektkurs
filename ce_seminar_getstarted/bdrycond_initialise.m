function prb=bdrycond_initialise(prb)

% function prb=bdrycond_initialise(prb)
% initialises the boundary conditions after reading the FEMM data structure
% this routine is independent from the FE mesh
%
% input parameters
%
% output parameters

for bd=1:length(prb.BdryProps)
  prb.bdrycond(bd).name=prb.BdryProps(bd).BdryName;
  prb.bdrycond(bd).type=sscanf(prb.BdryProps(bd).BdryType,'%d');
  switch prb.bdrycond(bd).type
    case 0                % Prescribed A
      prb.bdrycond(bd).type=0;
      prb.bdrycond(bd).value=sscanf(prb.BdryProps(bd).A_0,'%f');
      prb.bdrycond(bd).expression=[];
    case 1                % Small Skin Depth
    case 2                % Mixed
    case {3,4,5}          % (Anti-)periodic boundary conditions
      switch prb.bdrycond(bd).type
        case 3              % Strategic dual image (here use as the air-gap interface condition
          % the air-gap cut is introduced before, hence, there are a number of arcs present in the geometry
        case 4              % Periodic boundary condition
          prb.bdrycond(bd).X=1;
        case 5              % Anti-periodic boundary condition
          prb.bdrycond(bd).X=-1;
      end
    otherwise
      error('Unknown type %s for boundary condition\n',prb.bdrycond(bd).type,prb.bdrycond(bd).name);
  end
end
