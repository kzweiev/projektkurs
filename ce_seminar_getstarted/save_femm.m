function save_femm(prb,fout)

% function SAVE_FEMM(prb,fout)
% saves a FE-prb structure into a FEMM-formatted .ans file
%
% input parameters
%     prb            : FE-problem structure
%     fout           : file pointers
%
% output parameters
%
% see also READ_FEMM

% A. Adapt solution
if ~strcmp(prb.ProblemType,'axisymmetric')      % FEMM stores Az
  prb.node(:,3)=prb.node(:,3)/prb.Depth;
else                                            % FEMM stores 2*pi*r*Atheta
  % do nothing
end

% B. Rescale lengths
switch prb.LengthUnits
  case 'meters'
    lfac=1;
  case 'centimeters'
    lfac=100;
  case 'millimeters'
    lfac=1000;
  otherwise
    error('unit %s not not known\n',prb.LengthUnits);
end
prb.node(:,1:2)=prb.node(:,1:2)*lfac;
prb.points(:,1:2)=prb.points(:,1:2)*lfac;
prb.blocklabels(:,1:2)=prb.blocklabels(:,1:2)*lfac;
prb.Depth=prb.Depth*lfac;

% C. Readapt indices
prb.elem=prb.elem-1;
prb.segments(:,1:2)=prb.segments(:,1:2)-1;
prb.arcsegments(:,1:2)=prb.arcsegments(:,1:2)-1;

% D. Write info to file
fld=fieldnames(prb);
for q=1:length(fld) 
  switch fld{q}
    case {'Format','Frequency','Precision','MinAngle','LengthUnits','ProblemType','Coordinates','ACSolver','Comment','PointProps'}
      fprintf(fout,'[%s] = %s\n',fld{q},prb.(fld{q}));
    case 'Depth'                 % single double number
      fprintf(fout,'[%s] = %f\n',fld{q},prb.(fld{q}));
    case {'BdryProps','BlockProps','CircuitProps'}
      fprintf(fout,'[%s] = %d\n',fld{q},length(prb.(fld{q})));
      name=fld{q}(1:end-5);
      for p=1:length(prb.(fld{q}))
        fprintf(fout,'  <Begin%s>\n',name);
        subfld=fieldnames(prb.(fld{q}));
        for r=1:length(subfld)
          fprintf(fout,'    <%s> = %s\n',subfld{r},prb.(fld{q})(p).(subfld{r}));
        end
        fprintf(fout,'  <End%s>\n',name);
      end
    case 'points'
      fprintf(fout,'[NumPoints] = %d\n',size(prb.points,1));
      fprintf(fout,'%f %f %d %d\n',transpose(prb.points(:,1:4)));
    case 'segments'
      fprintf(fout,'[NumSegments] = %d\n',size(prb.segments,1));
      fprintf(fout,'%d %d %d %d %d %d\n',transpose(prb.segments(:,1:6)));
    case 'arcsegments'
      fprintf(fout,'[NumArcSegments] = %d\n',size(prb.arcsegments,1));
      fprintf(fout,'%d %d %f %d %d %d %d\n',transpose(prb.arcsegments(:,1:7)));
    case 'holes'
      fprintf(fout,'[NumHoles] = %d\n',size(prb.holes,1));
      fprintf(fout,'%f %f %f\n',transpose(prb.holes(:,1:3)));
    case 'blocklabels'
      fprintf(fout,'[NumBlockLabels] = %d\n',size(prb.blocklabels,1));
      fprintf(fout,'%d %d %d %d %d %d %d %d %d\n',transpose(prb.blocklabels(:,1:9)));
      % in the case of electric problems, the number of columns is different, then use the code below
      % numcol=size(prb.blocklabels,2);
      % fprintf(fout,[repmat('%d ',1,numcol) '\n'],transpose(prb.blocklabels(:,1:numcol)));
    case 'node'
      fprintf(fout,'[Solution]\n');
      fprintf(fout,'%d\n',size(prb.node,1));
      if sscanf(prb.Frequency,'%f')==0               % static case
        tmp=[prb.node(:,1:2) real(prb.node(:,3))];
        fprintf(fout,'%f %f %e\n',transpose(tmp));
      else                                           % time-harmonic case
        tmp=[prb.node(:,1:2) real(prb.node(:,3)) imag(prb.node(:,3))];
        fprintf(fout,'%f %f %e %e\n',transpose(tmp));
      end
    case 'elem'
      fprintf(fout,'%d\n',size(prb.elem,1));
      fprintf(fout,'%d %d %d %d\n',transpose(prb.elem(:,1:4)));
    case 'whatisthis'
      fprintf(fout,'%d\n',size(prb.whatisthis,1));
      if sscanf(prb.Frequency,'%f')==0               % static case
        tmp=[prb.whatisthis(:,1) real(prb.whatisthis(:,2))];
        fprintf(fout,'%d %e\n',transpose(tmp));
      else                                           % time-harmonic case
        tmp=[prb.whatisthis(:,1) real(prb.whatisthis(:,2)) imag(prb.whatisthis(:,2))];
        fprintf(fout,'%d %e %e\n',transpose(tmp));
      end
    otherwise
      % do nothing, HDG added some fields in the structure
      %fprintf(fout,'[%s] = %s\n',fld{q},prb.(fld{q}));
  end
end
