% evaluates the struct returned by runmeEval:
%% saves the struct in .mat-File
%% SOON saves result in .fem and .vtu Files
%% can make Matlab-Plots of the computed results

function []=evaluate(prb, plots)
%% plots [boolean] true:plots T... false: no plots
if nargin<2
    plots=false;
end


% saves A B E J T Q as .mat file for matlab and .vtk file for paraview
save('results\resultStruc.mat','-struct','prb','A','B','E','Q','T','J','error')
save_vtk (prb,'results/results.vtu',{'A_phasor',prb.A;'T_phasor',prb.T;'E_phasor',prb.E;}, {'B_phasor  ',prb.B;'J_phasor  ',prb.J;'Q_phasor ',prb.Q});

%Plots:
if plots==true 
% temperature:
subplot(2,2,1)
h= trimesh(prb.elem(:,1:3),prb.node(:,1),prb.node(:,2),prb.T);
title('Temperaturverteilung');
set(h,'FaceColor','interp');
set(h,'EdgeColor','interp');
view(2);
colorbar;

% magnetic vector potential
subplot(2,2,2)
k= trimesh(prb.elem(:,1:3),prb.node(:,1),prb.node(:,2),prb.A);
title('A-Phasor');
set(k,'FaceColor','interp');
set(k,'EdgeColor','interp');
view(2);

% maximum temperature for each material
subplot(2,2,3)
bar(prb.max_temp_regions)
title('Max Temp f�r Materialien');
set(gca,'XtickL',prb.regions)
% l = zoom;
% set(l,'Motion','vertical','Enable','on');


% electric field
subplot(2,2,4)
k= trimesh(prb.elem(:,1:3),prb.node(:,1),prb.node(:,2),prb.E);
title('E-Feld');
set(k,'FaceColor','interp');
set(k,'EdgeColor','interp');
view(2);
end

end