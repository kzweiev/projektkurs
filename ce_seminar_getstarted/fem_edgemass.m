function Mfem=fem_edgemass(prb,sigma)

% function Mfem=FEM_EDGEMASS(prb,sigma)
% returns the cff-edge-mass matrix for a certain region-wise constant conductivity
%
% input parameters
%       prb       : FEMM problem data structure
%       sigma     : region-wise or element-wise constant conductivity
%
% output parameters
%       Mfem      : FE mass matrix                                                                           numnode-by-numnode matrix
%
% see also DIVGRAD, CURLCURL, DIVGRAD

numnode=size(prb.node,1);                             % number of FE nodes
numelem=size(prb.elem,1);                             % number of FE elements

i=zeros(9*numelem,1);
j=zeros(9*numelem,1);
v=zeros(9*numelem,1);
[a,b,c,area,r,z]=linear_shape_functions(prb);
if size(sigma,1)~=numelem
  elemregi=prb.blocklabels(prb.elem(:,4),3);
  sigma=sigma(elemregi,:);
end
if strcmp(prb.ProblemType,'axisymmetric')
  depth=2*pi*mean(r,2);
else
  depth=prb.Depth*ones(numelem,1);
end
for k=1:numelem
  idx=prb.elem(k,1:3);
  rg=prb.blocklabels(prb.elem(k,4),3);
  
  bl=prb.elem(k,4);
  wr=prb.blocklabels(bl,5); 
  if (wr ~= 0)
      sigma(k) = 0;
  end
  
  %------------------------------------------------------------------------
  % IMPLEMENTATION POINT -DONE!
  % compute the elementary mass matrix 
  %------------------------------------------------------------------------
	elemmat=zeros(3,3);
	for counteri=1:3
        for counterj=1:3
            if (counteri==counterj)
                elemmat(counteri, counterj)=(1/6)*(sigma(k)/depth(k))*area(k);
            else
                elemmat(counteri, counterj)=(1/12)*(sigma(k)/depth(k))*area(k);
            end
        end
    end	

  i(9*(k-1)+[1:9],1)=reshape([idx; idx; idx],[],1);
  j(9*(k-1)+[1:9],1)=[idx idx idx]';
  v(9*(k-1)+[1:9],1)=reshape(elemmat,[],1);
end
Mfem=sparse(i,j,v,numnode,numnode);
