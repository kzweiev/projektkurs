function [Kfem]=feh_divgrad(prb,K)
% function Kfem=FEM_DIVGRAD(prb,K)
% returns the div-grad matrix for the region-wise or element-wise constant heat conductvity K
%
% input parameters
%    prb        : FEMM problem data structure
%    K          : heat conductivity (one per region or one per element)
%
% output parameters
%    Kfem       : div-grad matrix
%
% see also EDGEMASS, CURLCURL

numnode=size(prb.node,1);                             % number of FE nodes
numelem=size(prb.elem,1);                             % number of FE elements
i=zeros(9*numelem,1);
j=zeros(9*numelem,1);
v=zeros(9*numelem,1);
[a,b,c,area,r,z]=linear_shape_functions(prb);
lambda = K(prb.blocklabels(prb.elem(:,4),3),1);
if strcmp(prb.ProblemType,'axisymmetric')
  depth=2*pi*mean(r,2);
else
  depth=prb.Depth*ones(numelem,1);
end

for k=1:numelem
  idx=prb.elem(k,1:3);
  %------------------------------------------------------------------------
  % IMPLEMENTATION POINT - DONE
  % compute the elementary div-grad heat conductivity matrix
  %------------------------------------------------------------------------
  elemmat=zeros(3,3);
	for counteri=1:3
        for counterj=1:3
            elemmat(counteri,counterj)=(lambda(k)*depth(k)/(4*area(k)))*(c(k,counteri)*c(k,counterj)+b(k,counteri)*b(k,counterj));
        end
    end

  i(9*(k-1)+[1:9],1)=reshape([idx; idx; idx],[],1);    
  j(9*(k-1)+[1:9],1)=[idx idx idx]';
  v(9*(k-1)+[1:9],1)=reshape(elemmat,[],1);
end

Kfem = sparse(i,j,v,numnode,numnode);
