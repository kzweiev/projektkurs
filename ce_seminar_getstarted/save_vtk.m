function save_vtk (prb, filename, nodedata, elemdata)

% function SAVE_VTK(prb, filename, nodedata, elemdata)
% computes the coupling blocks for stranded conductors
%
% input parameters
%    prb        : FEMM problem data structure
%    file       : name of the target file (extension .vtu) 
%    nodedata   : cell array for node data. Each row contains a
%                 description string and an array of data located at the nodes 
%    elemdata   : cell array for element data. Each row contains a
%                 description string and an array of data located at the nodes 
%                 (numelem-by-1 or numelem-by-2)
%
% output parameters
%    Qv        : [W]       : heat generation matrix
%
% Example:
% prb.node = [0 0; 0 2; 2 0; 1 1; 2 2];
% prb.elem = [0 2 3; 1 0 3; 3 4 1; 2 4 3];
% pp1 = [1:5]'; pc1 = [1:4]'; pc2 = rand(4,2);
% save_vtk (prb,'exmpl.vtu',{'NodeData1',pp1},...
%              {'ElemData1',pc1;'RandomData',pc2})

%# initialize point data
if nargin<3
nodedata = {};
end

%# initialize cell data  
if nargin<4
elemdata = {};
end

%# get data and dimensions
node = prb.node;
elem = prb.elem;
numnode = size(node,1);
numelem = size(elem,1);

%# open file
fid = fopen (filename, 'w');

%# start writing if file was opened
if ( fid )

%# write header
fprintf (fid, '<?xml version="1.0"?>\n');
fprintf (fid, '<VTKFile type="UnstructuredGrid" version="0.1" byte_order="LittleEndian">\n');
fprintf (fid, '<UnstructuredGrid>\n');
fprintf (fid, '<Piece NumberOfPoints="%d" NumberOfCells="%d">\n',numnode,numelem);

%# start writing point data
fprintf (fid, '<PointData>\n'); 
for i=1:size(nodedata,1)
  %# write vector-valued data
  if (size(nodedata{i,2}) == [numnode,2])
    fprintf (fid, '<DataArray type="Float32" Name="%s" NumberOfComponents="3" format="ascii">\n',nodedata{i,1});
    fprintf (fid, '%g %g 0\n', nodedata{i,2}');
  %# write scalar-valued data
  elseif (size(nodedata{i,2}) == [numnode,1])
    fprintf (fid, '<DataArray type="Float32" Name="%s" format="ascii">\n',nodedata{i,1});
    fprintf (fid, '%g\n', nodedata{i,2}');
  %# dimension mismatch
  else
    error('save_vtk: pointdata size (%d) does not match number of points (%d)',size(nodedata{i,2},1),numnode);
  end
  fprintf (fid, '</DataArray>\n'); 
end
fprintf (fid, '</PointData>\n');

%# start writing cell data
fprintf (fid, '<CellData>\n');
for i=1:size(elemdata,1)
  %# write vector-valued data
  if (size(elemdata{i,2}) == [numelem,2])
    fprintf (fid, '<DataArray type="Float32" Name="%s" NumberOfComponents="3" format="ascii">\n',elemdata{i,1});
    fprintf (fid, '%g %g 0\n', elemdata{i,2}');
  %# write scalar-valued data
  elseif (size(elemdata{i,2}) == [numelem,1])
    fprintf (fid, '<DataArray type="Float32" Name="%s" format="ascii">\n',elemdata{i,1});
    fprintf (fid, '%g\n', elemdata{i,2}');  
  %# dimension mismatch
  else
    error('save_vtk: elemdata size (%d) does not match number of elements (%d)',size(elemdata{i,2},1),numnode);
  end
  fprintf (fid, '</DataArray>\n'); 
end
fprintf (fid, '</CellData>\n'); 

%# write mesh information (points)
fprintf (fid, '<Points>\n');
fprintf (fid, '<DataArray type="Float64" Name="Array" NumberOfComponents="3" format="ascii">\n');
fprintf (fid, '%g %g 0\n', node(:,1:2)');
fprintf (fid, '</DataArray>\n');
fprintf (fid, '</Points>\n');

%# write mesh information (cells)
fprintf (fid, '<Cells>\n');
fprintf (fid, '<DataArray type="Int32" Name="connectivity" format="ascii">\n');
if min(min(elem(:,1:3)))>0
  elem(:,1:3) = elem(:,1:3)-1;
else
  fprintf('save_vtk:    Node indeces start at 0.\n');  
end
fprintf (fid, '%d %d %d\n', elem(:,1:3)');
fprintf (fid, '</DataArray>\n');
fprintf (fid, '<DataArray type="Int32" Name="offsets" format="ascii">\n');
fprintf (fid, '%d\n', 3:3:(3*numelem));
fprintf (fid, '</DataArray>\n');
fprintf (fid, '<DataArray type="Int32" Name="types" format="ascii">\n');
fprintf (fid, '%d\n', 5*ones(numelem,1));
fprintf (fid, '</DataArray>\n');
fprintf (fid, '</Cells>\n');

%# write footer
fprintf (fid, '</Piece>\n');
fprintf (fid, '</UnstructuredGrid>\n');
fprintf (fid, '</VTKFile>');

%# close file
fclose (fid);

else

error(['save_vtk: could not open file ' filename]);

end %if
