function Kfem=fem_curlcurl(prb,relu)

% function Kfem=FEM_CURLCURL(prb,relu)
% returns the curl-curl matrix for the region-wise or element-wise constant reluctivities reginu
%
% input parameters
%    prb        : FEMM problem data structure
%    relu       : reluctivity (one per region or one per element)
%
% output parameters
%    Kfem       : curl-curl matrix
%
% see also EDGEMASS, DIVGRAD

numnode=size(prb.node,1);                             % number of FE nodes
numelem=size(prb.elem,1);                             % number of FE elements
i=zeros(9*numelem,1);
j=zeros(9*numelem,1);
v=zeros(9*numelem,1);
[a,b,c,area,r,z]=linear_shape_functions(prb);
if size(relu,1)~=numelem
  elemregi=prb.blocklabels(prb.elem(:,4),3);
  relu=relu(elemregi,:);
end

if size(relu,2)<2
  relu=[relu relu];
end
if strcmp(prb.ProblemType,'axisymmetric')
  depth=2*pi*mean(r,2);
else
  depth=prb.Depth*ones(numelem,1);
end
for k=1:numelem
  idx=prb.elem(k,1:3);
  %------------------------------------------------------------------------
  % IMPLEMENTATION POINT
  % compute the elementary curl-curl reluctance matrix -- Done! 
  %------------------------------------------------------------------------
	%isotrop material (both reluctivity-components are the same)
	nu=relu(k,1);      										
	elemmat=zeros(3,3);
	for counteri=1:3
        for counterj=1:3
            elemmat(counteri,counterj)=(nu/(4*area(k)*depth(k)))*(c(k,counteri)*c(k,counterj)+b(k,counteri)*b(k,counterj));
        end
    end

  
  i(9*(k-1)+[1:9],1)=reshape([idx; idx; idx],[],1);
  j(9*(k-1)+[1:9],1)=[idx idx idx]';
  v(9*(k-1)+[1:9],1)=reshape(elemmat,[],1);
end
Kfem=sparse(i,j,v,numnode,numnode);
