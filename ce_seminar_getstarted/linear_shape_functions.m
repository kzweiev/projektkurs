function [a,b,c,area,x,y,xc,yc]=linear_shape_functions(prb)

% function [a,b,c,area,x,y,C]=LINEAR_SHAPE_FUNCTIONS(mesh)
% returns for a list of elements, the corresponding nodal coordinates, linear shape functions and area's
%
% input parameters
%       mesh      : FE mesh
%
% output parameters
%       a         : shape function coefficients      n-by-3 array
%       b         : shape function coefficients      n-by-3 array
%       c         : shape function coefficients      n-by-3 array
%       area      : element area's                   n-by-1 vector
%       x         : x-coordinates of the nodes       n-by-3 array
%       y         : y-coordinates of the nodes       n-by-3 array
%       xc        : x-coordinates of the centers     n-by-1 vector
%       yc        : y-coordinates of the centers     n-by-1 vector
%
% see also

numelem=size(prb.elem,1);
x=reshape(prb.node(prb.elem(:,1:3),1),numelem,3);
y=reshape(prb.node(prb.elem(:,1:3),2),numelem,3);
x2=circshift(x,[0 -1]);
y2=circshift(y,[0 -1]);
x3=circshift(x,[0 -2]);
y3=circshift(y,[0 -2]);
a=x2.*y3-x3.*y2;
b=y2-y3;
c=x3-x2;

area = (a(:,1)+b(:,1).*x(:,1)+c(:,1).*y(:,1))/2;
xc   = sum(x,2)/3;
yc   = sum(y,2)/3;

idx=find(area<0);
if ~isempty(idx)
  fprintf('The following elements have a negative area, possibly because of wrong node orientation\n   ');
  fprintf(' %d',idx);
  figure(2); clf; trimesh(prb.elem(idx,1:3),prb.node(:,1),prb.node(:,2),'Color','k'); axis equal; xlabel('x (m)'); ylabel('y (m)'); title('mesh');
  error('\nFatal error\n\n');
end
